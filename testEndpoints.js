const assert = require("assert");
const frisby = require("frisby");

const hostname = process.env.HOSTNAME;
const port = process.env.PORT;

const base = "http://" + hostname + ":" + port;

assert(
	"Should say {a:1} with status 200",
	frisby
		.get(base + "/test")
		.expect("status", 200)
		.expect("json", "a", 1)
);