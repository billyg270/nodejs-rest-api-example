const express = require("express");
const fs = require("fs");

let app = express();

app.get("/test", function (req, res) {
	res.setHeader("Content-Type", "application/json");
	res.end(JSON.stringify({ a: 1 }));
});

let server = app.listen(8068, function () {
	let host = server.address().address;
	let port = server.address().port;
	console.log("Example app listening at http://%s:%s", host, port);
});